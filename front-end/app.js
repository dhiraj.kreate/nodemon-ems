var app = angular.module('loginApp',['ngRoute']);

const service_url = "http://localhost:4000/"; 



app.config([ '$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {   
        $routeProvider
		
		.when('/',{
		  		templateUrl : 'view/signup.html',
				controller : 'signupCtrl'
		})
		
		.when('/login',{
		  		templateUrl : 'view/login.html',
				controller : 'loginCtrl'
		})
		.when('/recovery',{
			templateUrl : 'view/recovery.html',
		  controller : 'dashCtrl'
  })
		.when('/signup',{
		  		templateUrl : 'view/signup.html',
				controller : 'signupCtrl'
		})
		.when('/dashboard',{
			templateUrl : 'view/dashboard.html',
		  controller : 'dashboardCtrl'
	})
	.when('/employee/',{
		templateUrl : 'view/employee.html',
		controller : 'employeeCtrl'
})
	.when('/employee/:profileId',{
		templateUrl : 'view/profile.html',
		controller : 'profileCtrl'
})
	.when('/logout',{
		templateUrl : 'view/login.html',
		controller : 'logoutCtrl'
	})
		
	} 
]);		

/* Create factory to Disable Browser Back Button only after Logout */
app.factory("checkAuth", function($location,$rootScope){
    return {
        getuserInfo : function(){
			if($rootScope.isLoggedIn === undefined || $rootScope.isLoggedIn === null){
				$location.path('/');
			}
		}
    };
});

app.controller('loginCtrl', function($scope,$location,$rootScope,$sce, $http,$timeout){
		$rootScope.isLoggedIn = false;
		
		
		$scope.submitLogin = function(){

				 

			var form_data=$scope.employee; 
		
			

							    
				$http({
					method: "POST",
					url: service_url+"users/authenticate",
					data:form_data, 
					headers : { 'Content-Type': 'application/json' } 
	
					      
					}).success(function(response) {

						//alert(response.status+" |"+response.message); 

						if (response.status != 'error') {
					        
							window.localStorage.setItem("SessionId", response._id);
							window.localStorage.setItem("SessionName", response.first_name);
							window.localStorage.setItem("isLoggedIn", true);
							window.localStorage.setItem("accessToken", response.token);
							
							$scope.responseStatus 		= "success";
							$scope.responseMessage 		= "Logged in successfully. You are being redirect to Dashboard..."; 
						 
							
							$timeout(function(){ 
								 
								$location.path("/dashboard"); 
							  },2000);

					       


					    } 
					    else   {
					       	  	
							$scope.responseStatus 		= 'error';
							$scope.responseMessage 		= response.message; 
					       
					    }
					});

 
				 
				
		} 
		
});


app.controller('signupCtrl', function($scope,$location,$rootScope,$sce, $http,$timeout){
	$rootScope.isLoggedIn = false;
	
	/* submitSignup Function to process registration data */
	
	
	$scope.submitRegistration=function(){
        /* while compiling form , angular created this object*/
		var form_data=$scope.employee; 

		alert(form_data); 

	  
		
		if(form_data.password!=form_data.repassword)
		{
			
			$scope.responseStatus 		='error';
			$scope.responseMessage 		= "Password doest not match. Please enter again"
			return false; 
		}

		
		$http({
			method: "POST",
			url: service_url+"users/register",
			data:form_data, 
			headers : { 'Content-Type': 'application/json' } 
			 

		}).success(function(response, status, headers, config) {

		 
			  


			if(response.status=='success')
			{
				$scope.responseStatus 		='success';
				$scope.responseMessage 		= response.message; 

				$scope.userName				= response.first_name; 

				$timeout(function(){ 
								 
					$location.path("/login"); 
				  },2000);

			}
			else
			{
				$scope.responseStatus 		='error';
				$scope.responseMessage 		= response.message; 
			}

			 

 
			
		});
		 
		        
    }
	
	
	
	 
	
});


 
	
app.controller('dashboardCtrl', function($scope,$location,$rootScope,$http,checkAuth){		
	
	$rootScope.SessionId 		= window.localStorage.getItem("SessionId");
	$rootScope.first_name 	= window.localStorage.getItem("SessionName");
	$rootScope.isLoggedIn 	= window.localStorage.getItem("isLoggedIn");
	$rootScope.accessToken 	= window.localStorage.getItem("accessToken");
	
	// Call checkAuth factory for cheking login details
	$scope.check = checkAuth.getuserInfo();
	
	$scope.page_title = "Dashboard"; 
	$scope.userName		=	$rootScope.first_name; 
 

 

	
});

app.controller('employeeCtrl', function($scope,$location,$rootScope,$http,checkAuth){		
	
	$rootScope.SessionId 		= window.localStorage.getItem("SessionId");
	$rootScope.first_name 	= window.localStorage.getItem("SessionName");
	$rootScope.isLoggedIn 	= window.localStorage.getItem("isLoggedIn");
	$rootScope.accessToken 	= window.localStorage.getItem("accessToken");
	
	// Call checkAuth factory for cheking login details
	$scope.check = checkAuth.getuserInfo();
	
	$scope.logout = function () {
			window.localStorage.clear();
			$rootScope.isLoggedIn = false;
			$location.path("/");
	};

	$scope.page_title = "Employee Directories"; 
	$scope.userName		=	$rootScope.first_name; 

	 
	$http({method: 'GET',url:service_url+'users',headers:{'Authorization': 'Bearer ' + $rootScope.accessToken}})
    	.success(function(response) {
            $scope.collection = response;
        })
    	.error(function() {
    		 console.log("error"); 
    	});

	
});

 
app.controller('profileCtrl', function($scope,$location,$rootScope,$http,checkAuth, $routeParams){		
	
	$rootScope.SessionId 		= window.localStorage.getItem("SessionId");
	$rootScope.first_name 	= window.localStorage.getItem("SessionName");
	$rootScope.isLoggedIn 	= window.localStorage.getItem("isLoggedIn");
	$rootScope.accessToken 	= window.localStorage.getItem("accessToken");
	
	// Call checkAuth factory for cheking login details
	$scope.check = checkAuth.getuserInfo();
	
	$scope.logout = function () {
			window.localStorage.clear();
			$rootScope.isLoggedIn = false;
			$location.path("/");
	};

	$scope.page_title = "Employee Details"; 
	$scope.userName		=	$rootScope.first_name; 

	var employeeProfileId= $routeParams.profileId; 

 
	$http({method: 'GET',url:service_url+'users/'+employeeProfileId,headers:{'Authorization': 'Bearer ' + $rootScope.accessToken}})
    	.success(function(response) {
						$scope.collection = response;

						console.log("profile details"); 
						console.log(response); 
						console.log("----------------"); 

						$scope.first_name 		= response.first_name
						$scope.last_name 			= response.last_name
						$scope.email 					= response.email
						$scope.contact_number = response.contact_number
						$scope.gender					= response.gender


						

        })
    	.error(function() {
    		 console.log("error"); 
    	});

	
});




app.controller('logoutCtrl', function($scope,$location,$rootScope,$sce, $http,$timeout){
 
	
	window.localStorage.clear();
	$rootScope.isLoggedIn = false;
	$location.path("/");
	
});




 
	
	
