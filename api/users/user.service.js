﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
nodeMailer = require('nodemailer');


const User = db.User;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function authenticate({ email, password }) {

    
    const user = await User.findOne({ email });
    if (user && bcrypt.compareSync(password, user.password)) {
        const { password, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secret);
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function getAll() {
    return await User.find().select('-hash');
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function create(req,res) {
    console.log("inside create Service. "); 
    console.log(req.body); 
    if (await User.findOne({ email: req.body.email })) {
       // throw 'Username "' + userParam.username + '" is already taken';
       res.status(200).send({status:'error', message:"Email already used. Please login"}); 

    }

    const user = new User(req.body);

    // hash password
    if (req.body.password) {
        user.password = bcrypt.hashSync(req.body.password, 10);
    }

    // save user
    //await user.save();

    await user.save().then(data => {

        var email_to = req.body.email; 
        var email_subject = "Welcome "+req.body.first_name;
        var email_content = "<b>Dear "+req.body.first_name+",  </b><br/> "
                            +" Welcome to EMS!"
                            +"<br/>We are all really excited to welcome you to our team! As agreed, your start date is Next Friday We expect you to be in our offices by 09:30 AM at Vaishali Office<br/>" 
                            +"<br/>Once again, congratulations, we are lucky to have you join us! We look forward to meeting you on your first day <br/><br/>Best regards, <br/>EMS."; 

        //_send_email(req, res, email_to,email_subject, email_content)

        

        //delete data["user_password"];
        //console.log(data); 

       // res.send(data);

    }).catch(err => {
        res.status(500).send({status:'error',
            message: err.message || "Some error occurred while creating the Note."
        });
    });


}


var _send_email = function(req, res, email_to,email_subject, email_content)
{

    /* start : send_email  */

    let transporter = nodeMailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: '--yourEmail--',
            pass: '--yourpassword--'
        }
    });
    let mailOptions = {
        from: '"Fitit Ltd" <fitit.branded@gmail.com>', // sender address
        to: email_to,  
        subject: email_subject, 
        text: email_subject, 
        html: email_content  
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
            res.render('index');
        });

    

}


async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}