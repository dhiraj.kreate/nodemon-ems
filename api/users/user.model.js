const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    email: { type: String, required: true }, 
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    password: { type: String, required: true },
    contact_code: { type: String, required: true },
    contact_number: { type: String, required: true },
    gender: { type: String, required: true },
    dob:{ type: Date, required: true },
    city:{ type: String, required: true },
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);

 